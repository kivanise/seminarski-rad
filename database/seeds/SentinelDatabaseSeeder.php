<?php

use Illuminate\Database\Seeder;

class SentinelDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create Users
        DB::table('users')->truncate();

        $admin = Sentinel::getUserRepository()->create(array(
            'first_name' => 'super',
            'last_name' => 'admin',
            'email'    => 'admin@admin.com',
            'password' => 'password'
        ));

        $user = Sentinel::getUserRepository()->create(array(
            'first_name' => 'plain',
            'last_name' => 'user',
            'email'    => 'user@user.com',
            'password' => 'password'
        ));

        // Create Activations
        DB::table('activations')->truncate();
        $code = Activation::create($admin)->code;
        Activation::complete($admin, $code);
        
        $code = Activation::create($user)->code;
        Activation::complete($user, $code);

        // Create Roles
        $administratorRole = Sentinel::getRoleRepository()->create(array(
            'name' => 'Administrator',
            'slug' => 'administrator',
            'permissions' => array(
                'users.create' => true,
                'users.update' => true,
                'users.view' => true,
                'users.destroy' => true,
                'roles.create' => true,
                'roles.update' => true,
                'roles.view' => true,
                'roles.delete' => true
            )
        ));
        $journalistRole = Sentinel::getRoleRepository()->create(array(
            'name' => 'Journalist',
            'slug' => 'journalist',
            'permissions' => array()
        ));

        // Assign Roles to Users
        $administratorRole->users()->attach($admin);
        $journalistRole->users()->attach($user);
    }
}
