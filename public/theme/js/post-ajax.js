/**
 * Created by znemcic1 on 1.12.2016..
 */
jQuery(document).ready(function ($) {
    $('#myform').submit(function (event) {
        event.preventDefault();

        var namelocal = $("input[name='name']").val();
        var tokenlocal = $("input[name='_token']").val();
        //alert(namelocal);
        $.ajax({
            url: '/izvjesca',
            type: 'POST',
            dataType: 'json',
            data: {
                'name': namelocal,
                '_token':tokenlocal
            },
            beforesend: function () {

            },
            success: function (data) {
                $('#message').html(data);
                // console.log(response);
            },
            error: function (error) {
                $('#message').html(error.responseText);
                // console.log(error);
            },
        });
    });
});