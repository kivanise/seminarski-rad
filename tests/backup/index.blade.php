@extends('Centaur::layout')
@section('title', 'Aktivnosti')

@section('content')

    @if(Session::has('message'))
        <div class="alert alert-success">{{Session::get('message')}}</div>
    @endif

    @if(Sentinel::check() && (Sentinel::inRole('administrator') || Sentinel::inRole('moderator') ))
        <center><a class="btn btn-warning" href="{{ route('aktivnosts.create') }}"><span
                        class="glyphicon glyphicon-plus"></span> Nova aktivnost</a></center>
    @endif


    <div class="row" style="z-index: 1000; width: 85%; margin: auto auto auto auto"> <!-- margin-left: 9% -->
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <center><h3 class="panel-title"><strong>PRIKAZ AKTIVNOSTI</strong></h3></center>
            {{--                 <!--<div class="pull-left"> Vaša ustrojstvena cjelina:<font color="#deb887"> {{ $uc_user }}</font> <br> Vaša--razina}}

        {{--                    pristupa:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#deb887"> {{ $role }}</font></div>--}}

            <!-- <div class="pull-left" style="margin-left: 20px;"><font color="red"> !</font> - žurna aktivnost <br>
                     <button class="btn-success"></button>
                     - završena aktivnost
                 </div>-->


                <div class="pull-right">
                    <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"> </span>
                        &nbsp; Filter
                    </button>
                </div>
            </div>
        <!--  <div style="height: 40px;text-align: center">
{{--                <center> Postotak izvršenosti (broj izvršenih / ukupan broj prikazanih) = {{ $posto }}%</center>--}}
                </div>
                <div class="progress progress-bar-info" style="width: 700px;position: relative; left: 30%;">
                    <div class="progress progress-bar-success" role="progressbar" aria-valuemin="0" aria-valuemax="100"
    {{--                     style="width: {{ $posto }}%;">--}}
                <span class="sr-only"></span>
            </div>
        </div>-->
            <table class="table table-hover">
                <thead style=" background-color: #CDCECD ">

                <tr class="filters">
                    <th class="colmd"></th>
                    <th><input type="text" class="form-control" placeholder="Aktivnost" disabled></th>
                    <th><input type="text" class="form-control" placeholder="Datum unosa" disabled></th>
                    <th><input type="text" class="form-control" placeholder="Nositelj" disabled></th>
                    {{--<th><input type="text" class="form-control" placeholder="Suradnici" disabled></th>--}}
                    {{--<th><input type="text" class="form-control" placeholder="Zadaća kolegij" disabled></th>--}}
                    {{--<th><input type="text" class="form-control" placeholder="Zadaću unio" disabled></th>--}}
                    {{--<th><input type="text" class="form-control" placeholder="Rok izvršenja" disabled></th>--}}
                    <th><input type="text" class="form-control" placeholder="Krajni rok" disabled></th>
                    <th><input type="text" class="form-control" placeholder="Status" disabled></th>
                    {{--<th><input type="text" class="form-control" placeholder="Realizirano" disabled></th>--}}
                    {{--<th><input type="text" class="form-control" placeholder="Realizaciju unio" disabled></th>--}}
                    <th><input type="text" class="form-control" placeholder="Kontrole" disabled></th>
                    @if(Sentinel::check() && (Sentinel::inRole('administrator') || Sentinel::inRole('moderator') ))
                        <th><input type="text" class="form-control" placeholder="Potvrda" disabled></th>
                    @endif

                </tr>
                </thead>
                <tbody>
                @foreach ($aktivnosts as $aktivnost)
                    @if($aktivnost->prikaz == 1)
                        <tr data-toggle="collapse" data-target="#according{{$aktivnost->id}}"
                            class="izvrseno"> {{--class="izvrseno"--}}
                    @else
                        <tr data-toggle="collapse" data-target="#according{{$aktivnost->id}}">
                            @endif
                            <td class="colmd">
                                @if($aktivnost->alert == 1)
                                    !
                                @else
                                    &nbsp;
                                @endif
                            </td>
                            <td class="table-text col-md-3">
                                <div style="color: #b03e00"><strong>{{ $aktivnost->aktivnost }}</strong></div>
                            </td>
                            <td class="table-text col-md-1">
                                <div>{{ date('d.m.Y', strtotime($aktivnost->datum_unos)) }}</div>
                            </td>
                            <td class="table-text col-md-2">
                                <div>{{ $aktivnost->naziv_uc }}</div>
                            </td>
                            {{--<td class="table-text col-md-1">--}}
                            {{--<div style="font-size: x-small;">{{ $aktivnost->suradnici }}</div>--}}
                            {{--</td>--}}
                            {{--<td class="table-text col-md-1">--}}
                            {{--<div>--}}
                            {{--<strong>{{ $aktivnost->kolegij}}</strong>/{{ date('d.m.Y', strtotime($aktivnost->kolegij_datum)) }}--}}
                            {{--</div>--}}

                            {{--</td>--}}
                            {{--<td class="table-text col-md-1">--}}
                            {{--<div>{{ $aktivnost->created_user }}</div>--}}
                            {{--</td>--}}

                            {{--<td class="table-text col-md-1">--}}
                            {{--@if(date('d.m.Y H:i:s', strtotime($aktivnost->rok ) - 259200) <  date('d.m.Y H:i:s'))--}}
                            {{--<div class="blink">{{ date('d.m.Y', strtotime($aktivnost->rok)) }}</div>--}}
                            {{--@else--}}
                            {{--<div>{{ date('d.m.Y', strtotime($aktivnost->rok)) }}</div>--}}
                            {{--{{  date('d.m.Y', strtotime($aktivnost->rok ) - '259200') }}--}}
                            {{--{{  date('d.m.Y') }}--}}
                            {{--@endif--}}
                            {{--</td>--}}
                            <td class="table-text col-md-1">
                                @if(strtotime($aktivnost->rok) === '0000-00-00 00:00:00')
                                    <div>{{ $aktivnost->termin }}</div>
                                @elseif($aktivnost->rok !== '0000-00-00 00:00:00' && (date('d.m.Y H:i:s', strtotime($aktivnost->rok )) <  date('d.m.Y H:i:s')) && ($aktivnost->status !== 'Izvršeno') )
                                    <div class="blink">{{ $aktivnost->termin }}</div>
                                    @else
                                    <div>{{ $aktivnost->termin }}</div>
                                @endif
                            </td>
                            <td class="table-text col-md-1">
                                @if($aktivnost->status == 'Izvršeno')
                                    <div class="izvrseno">{{ $aktivnost->status }}</div>
                                @else
                                    <div>{{ $aktivnost->status }}</div>
                                @endif
                            </td>
                            {{--<td class="table-text col-md-1">--}}
                            {{--<div>--}}
                            {{--@if($aktivnost->completed_at == null or $aktivnost->completed_at == '0000-00-00 00:00:00')--}}
                            {{-----}}
                            {{--@else--}}
                            {{--{{ date('d.m.Y', strtotime($aktivnost->completed_at)) }}--}}
                            {{--@endif--}}
                            {{--</div>--}}
                            {{--</td>--}}

                            {{--<td class="table-text col-md-1">--}}

                            {{--<div>{{ $aktivnost->edit_user }}</div>--}}

                            {{--</td>--}}

                        <!-- Controls-->
                            <td class="table-text col-md-1">
                                @if(Sentinel::check() && (Sentinel::inRole('administrator') || Sentinel::inRole('moderator') ))
                                    <a href="{{ route('aktivnosts.edit',$aktivnost->id) }}">
                                        <button class="btn-success btn-xs"><span
                                                    class="glyphicon glyphicon-edit"></span> Uredi
                                        </button>
                                    </a>

                                    {{--<a href="{{ route('aktivnosts.edit', ['aktivnosts' => $aktivnost->id]) }}" class="btn btn-default">Edit</a>--}}
                                    <form action="{{ route('aktivnosts.destroy', ['aktivnosts' => $aktivnost->id]) }}"
                                          method='POST' style="display: inline-block">

                                        {{ csrf_field() }}

                                        {{--{{link_to_route('aktivnosts.edit','<i class="glyphicon glyphicon-print"></i>Uredi',$aktivnost->id,['class'=>'btn btn-default'])}}--}}
                                        <input type="hidden" name='_method' value='DELETE'>
                                        {{--<input type="submit" class='btn btn-default' value='Arhiva'>--}}
                                        <button type="submit" class="btn-primary btn-xs"><span
                                                    class="glyphicon glyphicon-film"></span> Arhiva
                                        </button>

                                    </form>
                                @elseif(Sentinel::check() && Sentinel::inRole('subscriber'))
                                    {{--{{link_to_route('aktivnosts.edit2','Realizacija',$aktivnost->id,['class'=>'btn btn-default'])}}--}}
                                    <a href="{{ route('aktivnosts.edit2',$aktivnost->id) }}">
                                        <button class="btn-success btn-sm"><span class="glyphicon glyphicon-ok"></span>
                                            Realizacija
                                        </button>
                                    </a>
                                @endif

                            </td>
                            @if(Sentinel::check() && (Sentinel::inRole('administrator') || Sentinel::inRole('moderator') ))
                                <td class="colmd1" style="vertical-align:middle">
                                    @if( $aktivnost->status == 'Izvršeno' )
                                        @if($aktivnost->prikaz == 1)
                                            <a href="{{ route('aktivnosts.potvrda',$aktivnost->id) }}">
                                                <span class="glyphicon glyphicon-ok" style="color: #b03e00;"></span>
                                            </a>
                                        @else
                                            <a href="{{ route('aktivnosts.potvrda',$aktivnost->id) }}">
                                                <span class="glyphicon glyphicon-remove" style="color: #b03e00;"></span>
                                            </a>
                                        @endif
                                    @endif
                                </td>
                            @endif
                        </tr>
                        <tr>
                            <td colspan="8">
                                <div id="according{{$aktivnost->id}}" class="collapse">
                                    <table class="table table-bordered text-center" style="background-color: #c7ddef;">
                                        <tr>
                                            <td>Aktivnost:</td>
                                            <td style="color: #b03e00;"><strong>{{ $aktivnost->aktivnost }}</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Nositelj:</td>
                                            <td style="color: #2a62bc;">{{ $aktivnost->naziv_uc }}</td>
                                        </tr>
                                        <tr>
                                            <td>Suradnici:</td>
                                            <td  style="color: #2b542c;">{{ $aktivnost->suradnici }}</td>
                                        </tr>
                                        <tr>
                                            <td>Zadatak unio:</td>
                                            <td>{{$aktivnost->created_user}}</td>
                                        </tr>
                                        <tr>
                                            <td>Datum unosa:</td>
                                            <td>{{$aktivnost->datum_unos}}</td>
                                        </tr>
                                        <tr>
                                            <td>Kolegij:</td>
                                            <td>{{$aktivnost->kolegij_datum2}}</td>
                                        </tr>
                                        <tr>
                                            <td>Žurnost:</td>
                                            <td>{{$aktivnost->alert}}</td>
                                        </tr>
                                        <tr>
                                            <td>Kranji rok:</td>
                                            <td>{{$aktivnost->termin}}</td>
                                        </tr>
                                        <tr>
                                            <td>Upozorenje na dan:</td>
                                            <td>{{$aktivnost->rok}}</td>
                                        </tr>
                                        <tr>
                                            <td>Status aktivnosti:</td>
                                            <td>{{$aktivnost->status}}</td>
                                        </tr>
                                        <tr>
                                            <td>Status ažuriran:</td>
                                            <td>{{$aktivnost->completed_at}}</td>
                                        </tr>
                                        <tr>
                                            <td>Status ažurirao:</td>
                                            <td>{{$aktivnost->edit_user}}</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                </tbody>
            </table>
            @if(Sentinel::check() && (Sentinel::inRole('subscriber')))
                <center><h2 style="color: #1b6d85">Suradnik ste u aktivnostima:</h2></center>
                {{--Ako je suradnik u aktivnosti--}}
                <table class="table table-hover table-responsive" style="background-color: #f2dede;">
                    <thead>
                    <tr class="filters">
                        <th class="colmd"></th>
                        <th><input type="text" class="form-control" placeholder="Datum unosa" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Aktivnost" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Nositelj" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Suradnici" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Zadaća kolegij" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Zadaću unio" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Rok izvršenja" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Termin" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Status" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Realizirano" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Realizaciju unio" disabled></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($suraduje as $suradnik)
                        @if($suradnik->prikaz == 1)
                            <tr class="izvrseno">
                        @else
                            <tr>
                                @endif
                                <td class="colmd">
                                    @if($suradnik->alert == 1)
                                        !
                                    @else
                                        &nbsp;
                                    @endif
                                </td>
                                <td class="table-text col-md-1">
                                    <div>{{ date('d.m.Y', strtotime($suradnik->datum_unos)) }}</div>
                                </td>
                                <td class="table-text col-md-1">
                                    <div>{{ $suradnik->aktivnost }}</div>
                                </td>
                                <td class="table-text col-md-1">
                                    <div>{{ $suradnik->naziv_uc }}</div>
                                </td>
                                <td class="table-text col-md-1">
                                    <div style="font-size: x-small;">{{ $suradnik->suradnici }}</div>
                                </td>
                                <td class="table-text col-md-1">

                                    <div>
                                        <strong>{{ $suradnik->kolegij}}</strong>/{{ date('d.m.Y', strtotime($suradnik->kolegij_datum)) }}
                                    </div>

                                </td>
                                <td class="table-text col-md-1">
                                    <div>{{ $suradnik->created_user }}</div>
                                </td>

                                <td class="table-text col-md-1">
                                    @if(date('d.m.Y. H:i:s', strtotime($suradnik->rok ) - 259200) <  date('d.m.Y. H:i:s'))
                                        <div class="blink">{{ date('d.m.Y', strtotime($suradnik->rok)) }}</div>
                                    @else
                                        <div>{{ date('d.m.Y.', strtotime($suradnik->rok)) }}</div>
                                    @endif
                                </td>
                                <td class="table-text col-md-1">
                                    <div>{{ $suradnik->termin }}</div>
                                </td>
                                <td class="table-text col-md-1">
                                    @if($suradnik->status == 'Izvršeno')
                                        <div class="izvrseno">{{ $suradnik->status }}</div>
                                    @else
                                        <div>{{ $suradnik->status }}</div>
                                    @endif
                                </td>
                                <td class="table-text col-md-1">
                                    <div>
                                        @if($suradnik->completed_at == null or $suradnik->completed_at == '0000-00-00 00:00:00')
                                            -
                                        @else
                                            {{ date('d.m.Y', strtotime($suradnik->completed_at)) }}
                                        @endif
                                    </div>
                                </td>

                                <td class="table-text col-md-1">

                                    <div>{{ $suradnik->edit_user }}</div>

                                </td>

                                <!-- Controls-->

                            </tr>
                            @endforeach
                    </tbody>
                </table>
                <br><br>
            @endif

        </div>
    </div>











@endsection

<script src="{{ url('/theme/js/jquery-3.1.1.min.js') }}"></script>
<script>
    /*
     Please consider that the JS part isn't production ready at all, I just code it to show the concept of merging filters and titles together !
     */
    $(document).ready(function () {
        $('.filterable .btn-filter').click(function () {
            var $panel = $(this).parents('.filterable'),
                    $filters = $panel.find('.filters input'),
                    $tbody = $panel.find('.table tbody');
            if ($filters.prop('disabled') == true) {
                $filters.prop('disabled', false);
                $filters.first().focus();
            } else {
                $filters.val('').prop('disabled', true);
                $tbody.find('.no-result').remove();
                $tbody.find('tr').show();
            }
        });

        $('.filterable .filters input').keyup(function (e) {
            /* Ignore tab key */
            var code = e.keyCode || e.which;
            if (code == '9') return;
            /* Useful DOM data and selectors */
            var $input = $(this),
                    inputContent = $input.val().toLowerCase(),
                    $panel = $input.parents('.filterable'),
                    column = $panel.find('.filters th').index($input.parents('th')),
                    $table = $panel.find('.table'),
                    $rows = $table.find('tbody tr');
            /* Dirtiest filter function ever ;) */
            var $filteredRows = $rows.filter(function () {
                var value = $(this).find('td').eq(column).text().toLowerCase();
                return value.indexOf(inputContent) === -1;
            });
            /* Clean previous no-result if exist */
            $table.find('tbody .no-result').remove();
            /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
            $rows.show();
            $filteredRows.hide();
            /* Prepend no-result row if all rows are filtered */
            if ($filteredRows.length === $rows.length) {
                $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="' + $table.find('.filters th').length + '">No result found</td></tr>'));
            }
        });
    });

</script>




 