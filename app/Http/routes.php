<?php

// Authorization
Route::get('/login', ['as' => 'auth.login.form', 'uses' => 'Auth\SessionController@getLogin']);
Route::post('/login', ['as' => 'auth.login.attempt', 'uses' => 'Auth\SessionController@postLogin']);
Route::get('/logout', ['as' => 'auth.logout', 'uses' => 'Auth\SessionController@getLogout']);

// Users
Route::resource('users', 'UserController');

Route::resource('roles', 'RoleController');

Route::resource('/pages', 'PageController');

Route::get('/', ['as' => 'index', 'uses' => 'PageController@dashboard']);
