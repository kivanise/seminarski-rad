<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Sentinel;
use App\Page;

class PageController extends Controller
{

    public function __construct()
    {
        // Middleware
        $this->middleware('sentinel.auth', ['except' => ['show', 'dashboard']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Sentinel::getUser();
        return view('pages.index')->with('pages', Page::where('user_id', $user->id)->get());
    }

    public function dashboard() {

        $page = Page::orderBy('created_at', 'desc')->first();

        if($page) {
            return redirect()->to('/pages/' . $page->id);
        } else {
            return redirect('/pages');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            'name' => 'string|max:100',
            'content' => 'string',
            'photo' => 'required|image|mimes:jpeg,png,jpg',
        ]);

        $page = Page::create($request->all());
        $page->user_id = Sentinel::getUser()->id;


        if ($request->hasFile('photo')) {
            //  Let's do everything here
            if ($request->file('photo')->isValid()) {
                $photo = $request->file('photo');
                $name = time().'.'. $photo->getClientOriginalExtension();
                $destination = public_path('/images');
                //
                $photo->move($destination, $name);
                $page->photo = "/images/" . $name;
            }
        }

        $page->save();

        return redirect()->route('pages.index')->with('message', 'Uspješno ste kreirali stranicu!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = Page::findOrFail($id);

        return view('/pages.show', ['page' => $page]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::findOrFail($id);
        return view('pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $page = Page::findOrFail($id);

        $page->fill($request->all());

        if ($request->hasFile('photo')) {
            //  Let's do everything here
            if ($request->file('photo')->isValid()) {
                $photo = $request->file('photo');
                $name = time() . '.' . $photo->getClientOriginalExtension();
                $destination = public_path('/images');
                //
                $photo->move($destination, $name);
                $page->photo = "/images/" . $name;
            }
        }

        $page->save();

        session()->flash('success', 'Stranica uspješno ažurirana!');

        return redirect()->route('pages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::findOrFail($id);
        $page->delete();
        Session::flash('success', 'Stranica je obrisana!');
        return redirect()->route('pages.index');
    }
}
