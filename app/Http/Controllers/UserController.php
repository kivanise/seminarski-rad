<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Activation;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct()
    {
        // Middleware
        $this->middleware('sentinel.auth');
        $this->middleware('sentinel.access:users.create', ['only' => ['create', 'store']]);
        $this->middleware('sentinel.access:users.view', ['only' => ['index', 'show']]);
        $this->middleware('sentinel.access:users.update', ['only' => ['edit', 'update']]);
        $this->middleware('sentinel.access:users.destroy', ['only' => ['destroy']]);
        $this->userRepository = app()->make('sentinel.users');
    }

    public function index()
    {
        $users = $this->userRepository->createModel()->get();
        return view('Centaur::users.index', ['users' => $users]);
    } 

    public function create()
    {
        $roles = app()->make('sentinel.roles')->createModel()->all();
        return view('Centaur::users.create', ['roles' => $roles]);
    }

    public function store(Request $request)
    {
        $result = $this->validate($request, [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6'
        ]);
        $userArray = [
            'email' => trim($request->get('email')),
            'password' => $request->get('password'),
            'first_name' => $request->get('first_name', null),
            'last_name' => $request->get('last_name', null),
        ];

        $user = Sentinel::getUserRepository()->create($userArray);

        $code = Activation::create($user)->code;
        Activation::complete($user, $code);

        foreach ($request->get('roles', []) as $slug => $id) {
            $role = Sentinel::findRoleBySlug($slug);
            if ($role) {
                $role->users()->attach($user);
            }
        }

        session()->flash("success", "User {$request->get('email')} has been created.");
        return redirect('/users');
    }

    public function edit($id)
    {
        $user = $this->userRepository->findById($id);

        // Fetch the available roles
        $roles = app()->make('sentinel.roles')->createModel()->all();
         
        if ($user) {
            return view('Centaur::users.edit', [
                'user' => $user,
                'roles' => $roles,
            ]);
        }

        session()->flash('error', 'Invalid user.');
        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'password' => 'confirmed|min:6',
        ]);

        $attributes = [
            'email' => trim($request->get('email')),
            'first_name' => $request->get('first_name', null),
            'last_name' => $request->get('last_name', null)
        ];

        if ($request->has('password')) {
            $attributes['password'] = $request->get('password');
        }
        
        $user = $this->userRepository->findById($id);
        if (!$user) {
            session()->flash('error', 'Invalid user.');
            return redirect()->back()->withInput();
        }
        $user = $this->userRepository->update($user, $attributes);
        $roleIds = array_values($request->get('roles', []));
        $user->roles()->sync($roleIds);

        session()->flash('success', "{$user->email} ažuriran.");
        return redirect()->route('users.index');
    }

    public function destroy($id)
    {
        $user = $this->userRepository->findById($id);
        if (Sentinel::getUser()->id == $user->id) {
            $message = "You cannot remove yourself!";
            session()->flash('error', $message);
            return redirect()->route('users.index');
        }
        $user->delete();
        $message = "{$user->email} has been removed.";

        session()->flash('success', $message);
        return redirect()->route('users.index');
    }
}
