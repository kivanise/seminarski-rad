﻿<!DOCTYPE html>
<html lang="hr">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Seminarski rad za Backend Developera">
<meta name="author" content="Krešimir Ivanišević">

<title>@yield('naslov')</title>

<link href="knjige/css/bootstrap.css" rel="stylesheet"> 
<link href="knjige/css/3-col-portfolio.css" rel="stylesheet">
<link rel="icon" type="img/png" href="imgs/alcDigital.jpg">

@yield('head')






</head>
<body @yield('body')>
@yield('content')
<div class="container pull-xs-right">
        
</div>
@yield('afterbody')
</body>
</html>