@extends('Centaur::layout')
@section('title', $page->name )
@section('content')
    <div class="container-fluid">  
         <div class=" container" style="margin-top:50px">      
        <h1 style=" font-size:20px; color:#337ab7; font-weight:bold; margin:2%">{{ $page->name }}</h1>
        
       <div class="col-md-2"><img style="width:100%" src='{!! $page->photo !!}'/></div>
        <div class="col-md-10">
        <div>{{ $page->content }}</div>
        </div>
        </div>
    </div>
@endsection