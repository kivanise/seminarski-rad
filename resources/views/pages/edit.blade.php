@extends('Centaur::layout')
@section('title', 'Promjene')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                {{--display success message--}}
                @if(Session::has('success'))
                    <div class="alert alert-success">
                        Success: {{Session::get('success')}}
                    </div>
                @endif
                {{--display error message--}}
                @if($errors->has())
                    <ul class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading">Ažuriranje aktivnosti</div>
                    <div class="panel-body">
                        {!! Form::model($page, array('route'=>['pages.update',$page->id],'method'=>'PUT', "enctype"=>"multipart/form-data")) !!}
                        {{ csrf_field() }}

                        <div class="form-group">
                            {!! Form::label('name','Naziv stranice') !!}
                            {!! Form::text('name',null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                        {!! Form::label('content','Sadržaj stranice') !!}
                        {{ Form::textarea('content', null ,['class' => 'form-control']) }}
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('photo','Slika') !!}
                            <input id="photo" type="file" name="photo">
                        </div>

                            <center>
                                <div class="form-group">
                                    {!! Form::button('Spremi promjene',['type'=>'submit','class'=>'btn btn-success ']) !!}
                                    {{link_to_route('pages.index','Odustani',null,['class'=>'btn btn-danger '])}}
                                </div>
                            </center>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection