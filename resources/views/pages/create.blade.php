@extends('Centaur::layout')
@section('title', 'Kreiranje aktivnosti')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if($errors->has())
                    <ul class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading" align="center">Kreiranje stranice</div>
                    <div class="panel-body">

                        {!! Form::open(array('route'=>'pages.store', "enctype"=>"multipart/form-data")) !!}
                        <div class="form-group">
                            {!! Form::label('name','Naziv stranice') !!}
                            {!! Form::text('name',null,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('content','Sadržaj') !!}
                            {!! Form::textarea('content',null,['class'=>'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('photo','Slika') !!}
                            <input id="photo" type="file" name="photo">
                        </div>
                          <center>
                        <div class="form-group">
                          

                            {!! Form::button('Spremi',['type'=>'submit','class'=>'btn btn-primary']) !!}  
                           {{link_to_route('pages.index','Odustani',null,['class'=>'btn btn-danger'])}}
                          
                        </div></center>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

