@extends('Centaur::layout')

@section('title', 'Login')
 @if(Session::has('message'))
        <div class="alert alert-success">{{Session::get('message')}}</div>
    @endif
@section('content')


<div class="container" >
    <div class="row" style="margin-top: 15%;margin-left: -65px">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-default" style="background-color: #f5f5f5;">
				<div class="panel-heading">
					<center><h3 class="panel-title">Prijava</h3></center>
				</div>
				<div class="panel-body">
					<form accept-charset="UTF-8" role="form" method="post" action="{{ route('auth.login.attempt') }}">
					<fieldset>
						<div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
							<input class="form-control" placeholder="Korisničko ime" name="email" type="text" value="{{ old('email') }}">
							{!! ($errors->has('email') ? $errors->first('email', '<p class="text-danger">:message</p>') : '') !!}
						</div>
						<div class="form-group  {{ ($errors->has('password')) ? 'has-error' : '' }}">
							<input class="form-control" placeholder="Lozinka" name="password" type="password" value="">
							{!! ($errors->has('password') ? $errors->first('password', '<p class="text-danger">:message</p>') : '') !!}
						</div>
						<div class="checkbox">
							<label>
								<input name="remember" type="checkbox" value="true" {{ old('remember') == 'true' ? 'checked' : ''}}> Remember Me
							</label>
						</div>
						<input name="_token" value="{{ csrf_token() }}" type="hidden">
                                                <input class="btn btn btn-primary btn-block" style="" type="submit" value="Prijava">
					</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@stop

