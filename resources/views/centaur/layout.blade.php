<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csdf-token" content="{{ csrf_token() }}"/>
        <meta name="description" content="Seminarski rad tečaja Beckend Developer, Algebra"/>
        <meta name="author" content="Krešimir Ivanišević"/>
        <link rel="shortcut icon" href="favicon.ico">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>@yield('title')</title>

        <!-- Styles -->
        <link href="{{ url('/theme/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ url('/theme/css/style.css') }}" rel="stylesheet">
        <!-- Fonts -->
     
        <style type="text/css" media="all">
            /* fix rtl for demo */
            .chosen-rtl .chosen-drop {
                left: -9000px;
            }
        </style>
        @yield('skripte')


    </head>
    <body>
        <nav class="navbar  navbar-inverse  navbar-fixed-top">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">Najnovija stranica</a>
                    
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                Stranice<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                @foreach(\App\Page::all() as $page)
                                    <li class="{{ Request::is('pages/'. $page->id) ? 'active' : '' }}"><a href="{{ route('pages.show', $page->id) }}">{{ $page->name }} </a></li>
                                @endforeach
                            </ul>
                        </li>
                        @if ( Sentinel::check() && (Sentinel::inRole('administrator') OR Sentinel::inRole('journalist')))
                        <li class=""><a href="{{ route('pages.index') }}">Moje stranice</a></li>
                        @endif
                        @if ( Sentinel::check() && Sentinel::inRole('administrator') )
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                Admin<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li class="{{ Request::is('users*') ? 'active' : '' }}"><a href="{{ route('users.index') }}">Korisnici </a></li>
                                <li class="{{ Request::is('roles*') ? 'active' : '' }}"><a href="{{ route('roles.index') }}">Uloge</a></li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        @if (Sentinel::check())
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Sentinel::getUser()->email }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ route('auth.logout') }}"><i class="fa fa-btn fa-sign-out"></i>Odjava</a></li>
                            </ul>
                        </li>
                        @else
                        <li><a href="{{ route('auth.login.form') }}">Prijava</a></li>
                        @endif
                    </ul>
                </div>
                
                <!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <div id="okvir">
            <div id="scene3D">
                <div id="skisintro" style="position: absolute; top:70px"><img src="{{ url('/theme/images/ivanisevic_seminar.png') }}" width="90%"
                                                                    height="auto"></div>
            </div>
        </div>
        <div class="container" style="width: 100%; margin-top: -150px;">

            @include('Centaur::notifications')
            @yield('content')
        </div>
        <div id="footer" onclick="location.href = '#'">
            Copyright &copy; Seminarski rad Krešimir Ivanišević | Algebra Backend Developer |  - {{ date('Y') }}.
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="{{ url('/theme/js/jquery-3.1.1.min.js') }}"></script>
        <script src="{{ url('/theme/js/bootstrap.min.js') }}"></script>
        {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    <!-- Restfulizer.js - A tool for simulating put,patch and delete requests -->
    <script src="{{ asset('restfulizer.js') }}"></script>
</body>
@yield('afterbody')
</html>
<script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
</script>