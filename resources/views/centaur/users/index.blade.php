@extends('Centaur::layout')
@section('title', 'Users')
@section('content')

<div class="page-header">
    <div class="container" style="text-align: center;">
        
        <div class="row">
           
             

<div class='btn-toolbar pull-right'>
           <a href="{{ route('users.create') }}" class="btn btn-primary"> Dodaj korisnika </a>
        </div>
        <h1 style=" text-align:left">Korisnici</h1>
        




            <div class = "panel panel-primary filterable">
                <div class="panel-heading">
                    <center><h3 class="panel-title" style="color:white"><strong>Lista svih korisnika </strong></h3> </center>
                    
                    <div class="pull-right">
                        <button class="btn btn-default btn-xs btn-filter"><span style="margin-right: 10px;" class="glyphicon glyphicon-filter"> </span>Filter </button>
                    </div>
                </div>
                <table id="datatable" class="table table-hover" cellspacing="0" style=" background-color: white;">
                    <thead>
                        <?php $t = 1; ?>
                        <tr class = "filters" >
                            <th><input type = "text" class = "form-control" placeholder = "Rbr." disabled style = "text-align: center;"></th>
                            <th><input type = "text" class = "form-control" placeholder = "Ime i Prezime" disabled style = "text-align: center;"></th>
                            <th><input type = "text" class = "form-control" placeholder = "Razina prava" disabled style = "text-align: center;"></th>
                            <th><input type = "text" class = "form-control" placeholder = "Mogućnosti" disabled style = "text-align: center;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td> {{ $t++}}.  </td>
                            @if (!empty($user->first_name . $user->last_name))
                            <td><strong> {{ $user->first_name . ' ' . $user->last_name}} </strong></td>
                            @else
                            <td>(nema upisanog imena)</td>
                            @endif
                            <td> <strong>  @if ($user->roles)
                                    @if ($user->roles->count() > 0)
                                    {{ $user->roles->implode('name', ', ') }}
                                    @else
                                    <em class="label label-danger">Nema prava</em>
                                    @endif
                                    @endif
                                </strong>
                            </td>
                            <td>
                                <a href="{{ route('users.edit', $user->id) }}" class="btn btn-xs btn-info"> <span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Uredi </a>
                                <a href="{{ route('users.destroy', $user->id) }}" class="btn btn-xs btn-danger" data-method="delete" data-token="{{ csrf_token() }}"> <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Obriši </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>             
            </div>
        </div>
    </div>
    
</div>
@endsection
@section('afterbody')
<script src="{{ url('/theme/js/jquery-3.1.1.min.js') }}"></script>
<script>
/*
 Please consider that the JS part isn't production ready at all, I just code it to show the concept of merging filters and titles together !
 */
$(document).ready(function () {
    $('.filterable .btn-filter').click(function () {
        var $panel = $(this).parents('.filterable'),
                $filters = $panel.find('.filters input'),
                $tbody = $panel.find('.table tbody');
        if ($filters.prop('disabled') == true) {
            $filters.prop('disabled', false);
            $filters.first().focus();
        } else {
            $filters.val('').prop('disabled', true);
            $tbody.find('.no-result').remove();
            $tbody.find('tr').show();
        }
    });

    $('.filterable .filters input').keyup(function (e) {
        /* Ignore tab key */
        var code = e.keyCode || e.which;
        if (code == '9')
            return;
        /* Useful DOM data and selectors */
        var $input = $(this),
                inputContent = $input.val().toLowerCase(),
                $panel = $input.parents('.filterable'),
                column = $panel.find('.filters th').index($input.parents('th')),
                $table = $panel.find('.table'),
                $rows = $table.find('tbody tr');
        /* Dirtiest filter function ever ;) */
        var $filteredRows = $rows.filter(function () {
            var value = $(this).find('td').eq(column).text().toLowerCase();
            return value.indexOf(inputContent) === -1;
        });
        /* Clean previous no-result if exist */
        $table.find('tbody .no-result').remove();
        /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
        $rows.show();
        $filteredRows.hide();
        /* Prepend no-result row if all rows are filtered */
        if ($filteredRows.length === $rows.length) {
            $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="' + $table.find('.filters th').length + '">No result found</td></tr>'));
        }
    });
});

</script>
@endsection


